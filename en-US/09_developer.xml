<?xml version="1.0"?>
<chapter><title>Developer's Best Practices for OS Security</title>

<!-- This chapter is based on the patch I submitted to the Developer's
     Reference, see #337086: [BPP] Best practices for security design and review -->

<para>This chapter introduces some best secure coding practices for developers writing
Debian packages. If you are really interested in secure coding I recommend
you read David Wheeler's <ulink url="http://www.dwheeler.com/secure-programs/"
name="Secure Programming for Linux and Unix HOWTO" /> and 
<ulink url="http://www.securecoding.org" name="Secure Coding: Principles and Practices" />
by Mark G. Graff and Kenneth R. van Wyk (O'Reilly, 2003).</para>

<section id="bpp-devel-design">
   <title>Best practices for security review and design</title>

<para>Developers that are packaging software should make a best effort to ensure
that the installation of the software, or its use, does not introduce security
risks to either the system it is installed on or its users.

</para><para>In order to do so, they should make their best to review the source code of
the package and detect any flaws that might introduce security bugs before 
releasing the software or distributing a new version. It is acknowledged
that the cost of fixing bugs grows for different stages of its development, so
it is easier (and cheaper) to fix bugs when designing than when the software
has been deployed and is in maintenance mode (some studies say that the cost in
this later phase is <emphasis>sixty</emphasis> times higher). Although there
are some tools that try to automatically detect these flaws, developers
should strive to learn about the different kind of security flaws in
order to understand them and be able to spot them in the code they (or others)
have written.

</para><para>The programming bugs
which lead to security bugs typically include: <ulink
url="http://en.wikipedia.org/wiki/Buffer_overflow" name="buffer
overflows" />,
format string overflows,
heap overflows and
integer overflows (in C/C++ programs), temporary <ulink
url="http://en.wikipedia.org/wiki/Symlink_race" name="symlink race
conditions" /> (in scripts), <ulink
url="http://en.wikipedia.org/wiki/Directory_traversal" name="directory
traversal" /> and command injection (in servers) and <ulink
url="http://en.wikipedia.org/wiki/Cross_site_scripting"
name="cross-site scripting" />, and <ulink
url="http://en.wikipedia.org/wiki/SQL_injection" name="SQL
injection bugs" /> (in the case of web-oriented applications).
For a more complete information on security bugs review
Fortify's <ulink url="http://vulncat.fortifysoftware.com/" name="Taxonomy of 
Software Security Errors" />.

</para><para>Some of these issues might not be easy to spot unless you are an
expert in the programming language the software uses, but some security
problems are easy to detect and fix. For example, finding temporary
race conditions due to misuse of temporary directories can easily be done just by running
<literal>grep -r "/tmp/" .</literal>. Those calls can be reviewed and replace
the hardcoded filenames using temporary directories to calls to either
<command>mktemp</command> or <command>tempfile</command> in shell
scripts, <citerefentry><refentrytitle>File::Temp</refentrytitle> <manvolnum>3perl</manvolnum></citerefentry>
 in Perl scripts,
or <citerefentry><refentrytitle>tmpfile</refentrytitle> <manvolnum>3</manvolnum></citerefentry> in C/C++.

</para><para>There are a set of tools available to assist to the security code review phase.
These include <package>rats</package>, <package>flawfinder</package> and
<package>pscan</package>. For more information, read the 
<ulink url="http://www.debian.org/security/audit/tools" name="list of tools used
by the Debian Security Audit Team" />.

</para><para>When packaging software developers have to make sure that they follow
common security principles, including:

<itemizedlist>

<listitem><para>The software runs with the minimum privileges it needs:

<itemizedlist>
<listitem><para>The package does install binaries setuid or setgid.
<command>Lintian</command> will warn of <ulink url="
http://lintian.debian.org/reports/Tsetuid-binary.html" name="setuid" />,
<ulink url="http://lintian.debian.org/reports/Tsetgid-binary.html"
name="setgid" /> and <ulink
url="http://lintian.debian.org/reports/Tsetuid-gid-binary.html"
    name="setuid and setgid" /> binaries.</para></listitem>

<listitem><para>The daemons the package provide run with a 
low privilege user (see <xref linkend="bpp-lower-privs" />)</para></listitem>

</itemizedlist></para></listitem>

<listitem><para>Programmed (i.e., <command>cron</command>) tasks running in the
system do NOT run as root or, if they do, do not implement complex
tasks.</para></listitem>

</itemizedlist>

</para><para>If you have to do any of the above make sure the programs that
might run with higher privileges have been audited for security
bugs. If you are unsure, or need help, contact the <ulink
url="http://www.debian.org/security/audit/" name="Debian Security Audit
team" />. In the case of setuid/setgid binaries, follow the Debian
policy section regarding 
<ulink url="http://www.debian.org/doc/debian-policy/ch-files.html#s10.9"
name="permissions and owners" />

</para><para>For more information, specific to secure programming, make sure you
read (or point your upstream to) <ulink
url="http://www.dwheeler.com/secure-programs/" name="Secure Programming
for Linux and Unix HOWTO" /> and the <ulink
url="https://buildsecurityin.us-cert.gov/portal/" name="Build Security
In" /> portal. 
</para>
</section>

<!-- This should be explained here until #291177 gets fixed and this is
	added to policy -->
<section id="bpp-lower-privs">
    <title>Creating users and groups for software daemons</title>

<para>If your software runs a daemon that does not need root privileges,
you need to create a user for it. There are two kind of Debian users
that can be used by packages: static uids (assigned by
<package>base-passwd</package>, for a list of static users in Debian
see <xref linkend="faq-os-users" />) and dynamic uids in the range assigned
to system users.

</para><para>In the first case, you need to ask for a user or group id to the
<package>base-passwd</package>. Once the user is available there
the package needs to be distributed including a proper versioned depends to the
<package>base-passwd</package> package.

</para><para>In the second case, you need to create the system user either in
the <emphasis>preinst</emphasis> or in the <emphasis>postinst</emphasis> and make the package
depend on <literal>adduser (>= 3.11)</literal>.

</para><para>The following example code creates the user and group the daemon
will run as when the package is installed or upgraded:

<screen>
[...]
case "$1" in
  install|upgrade)

  # If the package has default file it could be sourced, so that
  # the local admin can overwrite the defaults

  [ -f "/etc/default/<varname>packagename</varname>" ] &#38;&#38; . /etc/default/<varname>packagename</varname>

  # Sane defaults:

  [ -z "$SERVER_HOME" ] &#38;&#38; SERVER_HOME=<varname>server_dir</varname>
  [ -z "$SERVER_USER" ] &#38;&#38; SERVER_USER=<varname>server_user</varname>
  [ -z "$SERVER_NAME" ] &#38;&#38; SERVER_NAME="<varname>Server description</varname>"
  [ -z "$SERVER_GROUP" ] &#38;&#38; SERVER_GROUP=<varname>server_group</varname>

  # Groups that the user will be added to, if undefined, then none.
  ADDGROUP=""

  # create user to avoid running server as root
  # 1. create group if not existing
  if ! getent group | grep -q "^$SERVER_GROUP:" ; then
     echo -n "Adding group $SERVER_GROUP.."
     addgroup --quiet --system $SERVER_GROUP 2>/dev/null ||true
     echo "..done"
  fi
  # 2. create homedir if not existing
  test -d $SERVER_HOME || mkdir $SERVER_HOME
  # 3. create user if not existing
  if ! getent passwd | grep -q "^$SERVER_USER:"; then
    echo -n "Adding system user $SERVER_USER.."
    adduser --quiet \
            --system \
            --ingroup $SERVER_GROUP \
            --no-create-home \
            --disabled-password \
            $SERVER_USER 2>/dev/null || true
    echo "..done"
  fi
  # 4. adjust passwd entry
  usermod -c "$SERVER_NAME" \
          -d $SERVER_HOME   \
          -g $SERVER_GROUP  \
             $SERVER_USER
  # 5. adjust file and directory permissions
  if ! dpkg-statoverride --list $SERVER_HOME >/dev/null
  then
      chown -R $SERVER_USER:adm $SERVER_HOME
      chmod u=rwx,g=rxs,o= $SERVER_HOME
  fi
  # 6. Add the user to the ADDGROUP group
  if test -n $ADDGROUP
  then
      if ! groups $SERVER_USER | cut -d: -f2 | \
         grep -qw $ADDGROUP; then
           adduser $SERVER_USER $ADDGROUP
      fi
  fi
  ;;
  configure)

[...]
</screen>

</para><para>You have to make sure that the init.d script file:

<itemizedlist>
<listitem><para>Starts the daemon dropping privileges: if the software does not
do the <citerefentry><refentrytitle>setuid</refentrytitle> <manvolnum>2</manvolnum></citerefentry> or 
<citerefentry><refentrytitle>seteuid</refentrytitle> <manvolnum>2</manvolnum></citerefentry>
 call itself, you can use the <literal>--chuid</literal>
call of <command>start-stop-daemon</command>.</para></listitem>

<listitem><para>Stops the daemon only if the user id matches, you can use the 
<command>start-stop-daemon</command> <literal>--user</literal> option
for this.</para></listitem>

<listitem><para>Does not run if either the user or the group do not exist:
<screen>
  if ! getent passwd | grep -q "^<varname>server_user</varname>:"; then
     echo "Server user does not exist. Aborting" >&#38;2
     exit 1
  fi
  if ! getent group | grep -q "^<varname>server_group</varname>:" ; then
     echo "Server group does not exist. Aborting" >&#38;2
     exit 1
  fi
</screen></para></listitem>

</itemizedlist>

</para><para>If the package creates the system user it can remove it when it is
purged in its <emphasis>postrm</emphasis>. This has some drawbacks, however.
For example, files created by it will be orphaned
and might be taken over by a new system user in the future if it is assigned
the same uid<footnote><para>Some relevant threads discussing these drawbacks
include <ulink url="http://lists.debian.org/debian-mentors/2004/10/msg00338.html" />
and <ulink url="http://lists.debian.org/debian-devel/2004/05/msg01156.html" /></para>
</footnote>. Consequently, removing system users on purge is
not yet mandatory and depends on the package needs. If unsure, this
action could be handled by asking the administrator for the prefered action
when the package is installed (i.e. through <command>debconf</command>).

</para><para>Maintainers that want to remove users in their <emphasis>postrm</emphasis> scripts
are referred to the <command>deluser</command>/<command>deluser</command> <literal>--system</literal>
option.

</para><para>Running programs with a user with limited privileges makes sure
that any security issue will not be able to damage the full system.
It also follows the principle of <emphasis>least privilege</emphasis>. Also consider you can
limit privileges in programs through other mechanisms besides running as
non-root<footnote><para>You can even provide a SELinux policy for
        it</para></footnote>. For more information, read the <ulink
url="http://www.dwheeler.com/secure-programs/Secure-Programs-HOWTO/minimize-privileges.html"
name="Minimize Privileges" /> chapter of the <emphasis>Secure Programming for
Linux and Unix HOWTO</emphasis> book.</para>

</section>

</chapter>
