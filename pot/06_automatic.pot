msgid ""
msgstr ""
"Project-Id-Version: 0\n"
"POT-Creation-Date: 2018-03-19 00:26+0100\n"
"PO-Revision-Date: 2018-03-19 00:26+0100\n"
"Last-Translator: Automatically generated\n"
"Language-Team: None\n"
"Language: en-US \n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Publican v4.3.2\n"

msgid "Automatic hardening of Debian systems"
msgstr ""

msgid "After reading through all the information in the previous chapters you might be wondering \"I have to do quite a lot of things in order to harden my system, couldn't these things be automated?\". The answer is yes, but be careful with automated tools. Some people believe, that a hardening tool does not eliminate the need for good administration. So do not be fooled to think that you can automate the whole process and will fix all the related issues. Security is an ever-ongoing process in which the administrator must participate and cannot just stand away and let the tools do all the work since no single tool can cope with all the possible security policy implementations, all the attacks and all the environments."
msgstr ""

msgid "Since woody (Debian 3.0) there are two specific packages that are useful for security hardening. The <package>harden</package> package which takes an approach based on the package dependencies to quickly install valuable security packages and remove those with flaws, configuration of the packages must be done by the administrator. The <package>bastille</package> package that implements a given security policy on the local system based on previous configuration by the administrator (the building of the configuration can be a guided process done with simple yes/no questions)."
msgstr ""

msgid "Harden"
msgstr ""

msgid "The <package>harden</package> package tries to make it more easy to install and administer hosts that need good security. This package should be used by people that want some quick help to enhance the security of the system. It automatically installs some tools that should enhance security in some way: intrusion detection tools, security analysis tools, etc. Harden installs the following <emphasis>virtual</emphasis> packages (i.e. no contents, just dependencies or recommendations on others):"
msgstr ""

msgid "<package>harden-tools</package>: tools to enhance system security (integrity checkers, intrusion detection, kernel patches...)"
msgstr ""

msgid "<package>harden-environment</package>: helps configure a hardened environment (currently empty)."
msgstr ""

msgid "<package>harden-servers</package>: removes servers considered insecure for some reason."
msgstr ""

msgid "<package>harden-clients</package>: removes clients considered insecure for some reason."
msgstr ""

msgid "<package>harden-remoteaudit</package>: tools to remotely audit a system."
msgstr ""

msgid "<package>harden-nids</package>: helps to install a network intrusion detection system."
msgstr ""

msgid "<package>harden-surveillance</package>: helps to install tools for monitoring of networks and services."
msgstr ""

msgid "Useful packages which are not a dependence:"
msgstr ""

msgid "<package>harden-doc</package>: provides this same manual and other security-related documentation packages."
msgstr ""

msgid "<package>harden-development</package>: development tools for creating more secure programs."
msgstr ""

msgid "Be careful because if you have software you need (and which you do not wish to uninstall for some reason) and it conflicts with some of the packages above you might not be able to fully use <package>harden</package>. The harden packages do not (directly) do a thing. They do have, however, intentional package conflicts with known non-secure packages. This way, the Debian packaging system will not approve the installation of these packages. For example, when you try to install a telnet daemon with <package>harden-servers</package>, <package>apt</package> will say:"
msgstr ""

msgid ""
"\n"
"# apt-get install telnetd \n"
"The following packages will be REMOVED:\n"
"  harden-servers\n"
"The following NEW packages will be installed:\n"
"  telnetd \n"
"Do you want to continue? [Y/n]"
msgstr ""

msgid "This should set off some warnings in the administrator head, who should reconsider his actions."
msgstr ""

msgid "Bastille Linux"
msgstr ""

msgid "<ulink name=\"Bastille Linux\" url=\"http://bastille-linux.sourceforge.net/\" /> is an automatic hardening tool originally oriented towards the Red Hat and Mandrake Linux distributions. However, the <package>bastille</package> package provided in Debian (since woody) is patched in order to provide the same functionality for Debian GNU/Linux systems."
msgstr ""

msgid "Bastille can be used with different frontends (all are documented in their own manpage in the Debian package) which enables the administrator to:"
msgstr ""

msgid "Answer questions step by step regarding the desired security of your system (using <citerefentry><refentrytitle>InteractuveBastille</refentrytitle> <manvolnum>8</manvolnum></citerefentry>"
msgstr ""

msgid "Use a default setting for security (amongst three: Lax, Moderate or Paranoia) in a given setup (server or workstation) and let Bastille decide which security policy to implement (using <citerefentry><refentrytitle>BastilleChooser</refentrytitle> <manvolnum>8</manvolnum></citerefentry>)."
msgstr ""

msgid "Take a predefined configuration file (could be provided by Bastille or made by the administrator) and implement a given security policy (using <citerefentry><refentrytitle>AutomatedBastille</refentrytitle> <manvolnum>8</manvolnum></citerefentry>)."
msgstr ""

